<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function showUsers()
    {
        $users = User::all();

        return view('', compact('users')); // View CRUD User awal
    }

    public function formCreateUser()
    {
        return view(''); // View form buat bikin user
    }

    public function userStore(Request $request)
    {
        $request->validate([
            'nama'      =>  'required',
            'nim'       =>  'min:12|max:12|unique:users',
            'email'     =>  'required|unique:users',
            'password'  =>  'required|min:6',
            'role'      =>  'required'
        ]);

        $requestData = $request->all();
        $requestData['password'] = bcrypt($requestData['password']);

        User::create($requestData);

        return redirect(''); // VIEW CRUD USER AWAL
    }

    public function formEditUser(User $user)
    {
        return view('', compact('user')); // view form edit
    }

    public function userUpdate(Request $request, User $user)
    {
        $request->validate([
            'nama'      =>  'required',
            'nim'       =>  'min:12|max:12|unique:users'.$user->id,
            'email'     =>  'required|unique:users'.$user->id,
            'role'      =>  'required'
        ]);

        $user->update($request->all());

        return redirect(''); // View CRUD USER AWAL
    }

    public function userDestroy(User $user)
    {
        $user->delete();

        return redirect(''); // VIEW CRUD USER AWAL
    }
}
