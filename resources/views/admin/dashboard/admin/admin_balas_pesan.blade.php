@extends('admin.dashboard.layouts.main')

@php
    $title = 'Admin';
@endphp

@section('title')
    Dashboard Balas Pesan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
        <form method="POST" action="{{ route('kirim.jawaban', $question->id) }}">
            @csrf
            <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Balas Pertanyaan</h1>

            <div class="pesan d-flex text-dark text-decoration-none px-2 py-3 border-bottom">
                <div class="col-2">
                    <img src="{{ asset('img/profile.png') }}" alt="Foto Profil" class="d-block mx-auto" style="width: 45px">
                </div>
                <div class="col-8 px-2">
                    <h1 class="fs-6">{{ $question->user->nama }}</h1>
                    <p class="m-0">{{ $question->question_content }}</p>
                </div>
                <div class="col-2 d-flex flex-column justify-content-start align-items-center">
                    <small class="text-center">{{ $question->created_at->format('d F Y') }}</small>
                </div>
            </div>

            <div class="mt-4">
                <label for="jawaban" class="form-label fw-semibold">Jawaban</label>
                <textarea class="form-control" id="jawaban" name="jawaban" rows="5" placeholder="Masukkan Jawaban"></textarea>
            </div>

            <div class="d-flex justify-content-end mt-5">
                <button type="submit" class="btn btn-dark px-5 py-2">Kirim</button>
            </div>
        </form>
    </div>
@endsection