document.getElementById('deleteAvatar').addEventListener('click', function () {
  const avatarPreview = document.getElementById('file-preview');
  avatarPreview.src = '{{ asset("avatars/default.png") }}';
  const inputAvatar = document.getElementById('avatar');
  inputAvatar.value = null;
});